source /Library/Developer/CommandLineTools/usr/share/git-core/git-prompt.sh
source /Library/Developer/CommandLineTools/usr/share/git-core/git-completion.bash


export PS1='\u:\w\[\033[01;35m\]$(__git_ps1 " [%s]")\[\033[00m\]$ '
export PATH=~/Library/Python/2.7/bin:$PATH
export NIX_PATH=:nixpkgs=/Users/samuelwilliams/.nix-defexpr/channels/nixos-17.03
export DM_CREDENTIALS_REPO=/Users/samuelwilliams/git/digitalmarketplace-credentials

if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi

export LSCOLORS=ExFxBxDxCxegedabagacad
alias ls='ls -GFh'

alias grep='grep --color=auto'

eval "$(direnv hook bash)"
eval "$(rbenv init -)"

source ~/dotfiles/aliases.sh
source ~/.dm-dev-utils/python-auto-venv
source ~/.dm-dev-utils/git-aliases.sh
source ~/.dm-dev-utils/dm-aliases.sh

test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"
export PATH="/usr/local/opt/postgresql@9.5/bin:$PATH"
export PATH="/Users/samuelwilliams/.gem/ruby/2.3.1/bin:$PATH"
export PATH="/usr/local/opt/nodejs/bin:$PATH"
