export VIRTUAL_ENV_DISABLE_PROMPT=1
export PS1='\u:\w\[\033[01;35m\]$(__git_ps1 " [%s]")\[\033[00m\] $'
export PATH=~/Library/Python/2.7/bin:$PATH

# Run these only on macOS
if which brew > /dev/null; then
  export LSCOLORS=ExFxBxDxCxegedabagacad
  alias ls='ls -GFh'

  if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
  fi
fi

function venv_stuff {
  if [ -d "venv" ]; then
    if [ ${VIRTUAL_ENV} ]; then
      deactivate
    fi

    source venv/bin/activate
  elif [ "$1" = "make" ]; then
    virtualenv venv
    source venv/bin/activate
  else
    echo "Syntax: VENV [MAKE]"
  fi
}

function venv3_stuff {
  if [ -d "venv3" ]; then
    if [ ${VIRTUAL_ENV} ]; then
      deactivate
    fi

    source venv3/bin/activate
  elif [ "$1" = "make" ]; then
    virtualenv --python=python3 venv3
    source venv3/bin/activate
  else
    echo "Syntax: VENV3 [MAKE]"
  fi
}

function cd_with_venv {
  command cd "$@"

  if [ ${VIRTUAL_ENV} ]; then
    deactivate > /dev/null 2>&1
  fi

  CURRENT_DIR=`pwd`
  while [ "${CURRENT_DIR}" != "/" ]; do
    if [ -d "${CURRENT_DIR}/venv" ]; then
      command cd "${CURRENT_DIR}" > /dev/null
      venv_stuff
      command cd - > /dev/null 2>&1
      break
    elif [ -d "${CURRENT_DIR}/venv3" ]; then
      command cd "${CURRENT_DIR}" > /dev/null
      venv3_stuff
      command cd - > /dev/null 2>&1
      break
    fi

    CURRENT_DIR=`dirname ${CURRENT_DIR}`
  done
}

alias grep='grep --color=auto'
alias venv=venv_stuff
alias venv3=venv3_stuff
alias cd=cd_with_venv

eval "$(direnv hook bash)"
